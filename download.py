#!/usr/bin/python3

import sys
import urllib.request as ulr
import urllib.error as ule

def download_file(url, target, etag):
    request = ulr.Request(url)
    request.add_header('User-Agent', 'curl/7.71.0')
    if len(etag) > 0:
        request.add_header('If-None-Match', etag)
    try:
        with ulr.urlopen(request) as response:
            etag = response.getheader('ETag')
            with open(target, mode = 'wb') as file:
                buffer = bytearray(16 * 1024)
                for count in iter(lambda: response.readinto(buffer), 0):
                    file.write(buffer[0:count])
            return etag
    except ule.HTTPError as e:
        if e.code == 304:
            return etag
        else:
            raise

url = sys.argv[1]
target = sys.argv[2]
etag = ''
if len(sys.argv) >= 4:
    etag = sys.argv[3]
etag = download_file(url, target, etag)
print(etag)
