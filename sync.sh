#!/bin/bash

set -e

echo 'Downloading pMetroSetup.exe'
etag="`cat 'etag' || true`"
etag="`./download.py \
'http://pmetro.su/download/pMetroSetup.exe' 'setup.exe' "$etag"`"
[ -f 'setup.exe' ] || {
  echo 'pMetroSetup.exe did not change'
  exit 0
}

apt-get update && apt-get -y upgrade &&
apt-get install -y --no-install-recommends \
innoextract zip python3-pip

innoextract 'setup.exe'
[ -d 'app' ]

dir="`pwd`"
git -C '/opt' clone 'https://github.com/RomanGolovanov/ametro-services'
cd '/opt/ametro-services'
patch -Np1 -i "$dir/services.patch"

pushd 'manual'
innoextract "$dir/setup.exe"
[ -d 'app' ]
popd

mkdir 'geonames'
pushd 'geonames'
curl -LO 'https://download.geonames.org/export/dump/countryInfo.txt'
curl -LO 'https://download.geonames.org/export/dump/cities1000.zip'
curl -LO 'https://download.geonames.org/export/dump/alternateNames.zip'
zip 'countryInfo.zip' 'countryInfo.txt'
rm 'countryInfo.txt'
popd

pip3 install -r 'requirements.txt'
python3 ./run-all.py
rm -rf "$dir/repo"
mv 'publish' "$dir/repo"

cd "$dir"
./validate.py
[ -z "$etag" ] || echo "$etag" > 'etag'
git add 'repo' 'etag'
git commit -m 'Update repo'
