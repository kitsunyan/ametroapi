#!/usr/bin/python3

import json
import os
import subprocess
import zipfile

repo_dir = 'repo'

def filter_values(target, key, values):
    target[key] = [value for value in target[key] if value in values]

def handle_index(index):
    # remove missing transports which cause aMetro to crash
    transport_types = [t.get("name") for t in index['transports']]
    for scheme in index['schemes']:
        filter_values(scheme, 'default_transports', transport_types)
        filter_values(scheme, 'transports', transport_types)
    return index

for name in os.listdir(repo_dir):
    if name.endswith('.zip'):
        path = os.path.join(repo_dir, name)
        file = zipfile.ZipFile(path)
        original = file.read('index.json').decode('utf-8')
        modified = json.dumps(handle_index(json.loads(original)),
            ensure_ascii = False, indent = 4)
        file.close()
        if original != modified:
            subprocess.check_call(['zip', '-d', path, 'index.json'])
            with zipfile.ZipFile(path, 'a') as file:
                file.writestr('index.json', modified)
